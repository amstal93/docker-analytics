FROM rocker/geospatial

MAINTAINER Victor Santos <victor_santos@fisica.ufc.br>

RUN Rscript -e "install.packages(c('tidyverse', 'rmarkdown', 'workflowr', 'flexdashboard', 'plotly', 'RColorBrewer', 'ggsci', 'viridis', 'devtools')); library('devtools'); devtools::install_gitlab('krpack/arango-driver')"
COPY .Rprofile /home/rstudio/
